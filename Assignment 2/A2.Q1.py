import numpy as np
import pandas as pd
import random as rd
import matplotlib.pyplot as plt
data = pd.read_csv('/home/chitranshu/my_project_dir/Assignment 2/data.csv')
X = data.iloc[:, 0]
Y = data.iloc[:, 1]

#initialising the parameters
m = 1 #theta0
c = 1 #theta1
n = int(len(Y))
#feature scaling of data
x_fs = (X-np.mean(X))/(max(X) - min(X))
y_fs = (Y-np.mean(Y))/(max(Y) - min(Y))

#spliting data into training and test data sets
x_train=x_fs[:round(n*0.7)]
x_test=x_fs[round(n*0.7):]
y_train=y_fs[:round(n*0.7)]
y_test=y_fs[round(n*0.7):]

#plotting training and test data on a same plot
plt.scatter(x_train,y_train)
plt.scatter(x_test,y_test)

#temporary variable for theta0 and theta 1
temp_m = 0
temp_c = 0

i = 0 #To find the number of iterations to reach the desired the accuracy
cost=[] #Empty list to store value of cost function after each iteration

print("Iteration","\tCost Function")

alpha = 0.5  # The learning Rate
#iterations = 1000  # The number of iterations to perform gradient descent
#z = 0
# Performing Gradient Descent
test =[]
while abs(temp_m-m)>0.000001 and abs(temp_c-c)>0.000001:
    Y_pred = m*x_train + c  # The current predicted value of Y
    D_m = (-1/n) * sum(x_train * (y_train - Y_pred))  # Derivative wrt m
    D_c = (-1/n) * sum(y_train - Y_pred)  # Derivative wrt c
    ct=1/(2*n)*sum((Y_pred-y_train)**2) #cost function
    cost.append(ct)
    temp_m = m
    temp_c = c
    m = m - alpha * D_m  # Update m
    c = c - alpha * D_c  # Update c
    Y_test_pred = m*x_test + c
    test.append(np.mean((Y_test_pred - y_test)**2))
    i+=1
    print(i,'\t->\t',ct)
print("Number of iterations = ",i)
print('Theta1= ',m)
print('Theta0= ',c)

#Plotting regression line in plot1
y_pred=m*x_train+c
plt.xlabel("x")
plt.ylabel("y")
plt.plot(x_train,y_pred,'-r')
plt.show()

#Plotting cost vs Number of iterations
plt.xlabel("Number of iterations")
plt.ylabel("Cost Function")
plt.plot(range(1,i+1),cost)
plt.plot(range(1,i+1),test,'-r')
plt.show()


