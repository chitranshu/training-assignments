import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import random as rd
data=pd.read_csv("/home/chitranshu/my_project_dir/Assignment 2/data.csv")
x=(data.iloc[:,0])
y=(data.iloc[:,1])

#Initializing the parameters
theta0=1 
theta1=1

n=int(len(y))
#Feature Scaling the entire Data
mean_x=np.mean(x)
range_x=max(x)-min(x)
fs_x=(x-mean_x)/range_x

mean_y=np.mean(y)
range_y=max(y)-min(y)
fs_y=(y-mean_y)/range_y

#Splitting the data into Test data and Train data
x_train=fs_x[:round(n*0.7)]
x_test=fs_x[round(n*0.7):]
y_train=fs_y[:round(n*0.7)]
y_test=fs_y[round(n*0.7):]

#Plotting scatter plot of train and test data on the same plot
#Let's call this plot1
plt.scatter(x_train,y_train)
plt.scatter(x_test,y_test)

#Learning rate
alpha=0.5

#Temporary variable to store theta0 and theta1
temp_theta1=0
temp_theta0=0

i=0 #To find the number of iterations to reach the desired the accuracy
cost=[] #Empty list to store value of cost function after each iteration

print("Iteration","\tCost Function")

# Loop to find the value of theta0 and theta1 with a precision of 10^-6
while abs(temp_theta1-theta1)>0.000001 and abs(temp_theta0-theta0)>0.000001:
   y_pred=theta1*x_train+theta0 #Similar to y=mx+c
   #Differetiating the cost functoin J wrt theta1
   D_theta1=(-1/n)*sum(x_train*(y_train-y_pred)) 
   #Differetiating the cost functoin J wrt theta0
   D_theta0=(-1/n)*sum((y_train-y_pred))
   #Calculating the cost function
   ct=1/(2*n)*sum((y_pred-y_train)**2)
   cost.append(ct)
   #Storing previous value of theta1 and theta0
   temp_theta1=theta1
   temp_theta0=theta0
   #Updating theta1 and theta0 
   theta1=theta1-alpha*D_theta1
   theta0=theta0-alpha*D_theta0
   i+=1
   print(i,'\t->\t',ct)
print("Number of iterations = ",i)
print('Theta1= ',theta1)
print('Theta0= ',theta0)

#Plotting regression line in plot1
y_pred=theta1*x_train+theta0
plt.xlabel("x")
plt.ylabel("y")
plt.plot(x_train,y_pred,'-r')
plt.show()

#Plotting cost vs Number of iterations
plt.xlabel("Number of iterations")
plt.ylabel("Cost Function")
plt.plot(range(1,i+1),cost)
plt.show()