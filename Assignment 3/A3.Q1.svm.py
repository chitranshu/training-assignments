import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
#from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import accuracy_score
from sklearn import tree
from sklearn import preprocessing
#from sklearn import datasets
#import graphviz
from sklearn.ensemble import RandomForestRegressor
from sklearn import metrics
from sklearn.model_selection import cross_val_score
from sklearn.preprocessing import StandardScaler as sc
from sklearn import svm
from sklearn.model_selection import GridSearchCV

data = pd.read_csv('/home/chitranshu/Documents/sonar.all-data', sep= ',', header= None)
label_encoder = preprocessing.LabelEncoder()
data.iloc[:,60]= label_encoder.fit_transform(data.iloc[:,60])
print(data.iloc[:,60].unique())

X = data.values[:, :60]
Y = data.values[:, 60]
params = {'kernel' : ('linear', 'poly', 'rbf'), 'C' : [0.01, 0.1, 1, 10], 'gamma' : [0.01, 0.1, 1, 10]}
X_train, X_test, y_train, y_test = train_test_split( X, Y, test_size = 0.25, random_state = 42, shuffle= True)
clf = svm.SVC(probability=True)

grid = GridSearchCV(clf, params, verbose=1, cv=5)
grid.fit(X_train, y_train)
y_pred = grid.predict(X_test)
print(grid.best_estimator_)

#Train the model using the training sets
#clf.fit(X_train, y_train)

#Predict the response for test dataset
#y_pred = clf.predict(X_test)
train_pred = grid.predict(X_train)
# Model Accuracy: how often is the classifier correct?
print("Train Accuracy : ", metrics.accuracy_score(y_train,train_pred))
print("Accuracy:",metrics.accuracy_score(y_test, y_pred))
print("Precision:",metrics.precision_score(y_test, y_pred))
print("Recall:",metrics.recall_score(y_test, y_pred))

val_score1 = cross_val_score(grid.best_estimator_, X, Y, cv = 5)
accu1 = val_score1.mean()
print("cross validation for svm is : ", accu1)
svc_prob = grid.predict_proba(X_test)[:, 1]
roc_val = metrics.roc_auc_score(y_test, svc_prob)
print(roc_val)