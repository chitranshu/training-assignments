import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import accuracy_score
from sklearn import tree
from sklearn import datasets
import graphviz
from sklearn.model_selection import cross_val_score


data = pd.read_csv('/home/chitranshu/Documents/sonar.all-data', sep= ',', header= None)
X = data.values[:, :60]
Y = data.values[:, 60]
#print (X,Y)
X_train, X_test, y_train, y_test = train_test_split( X, Y, test_size = 0.25, random_state = 50, shuffle= True)

classifier1 = DecisionTreeClassifier(criterion = "gini", random_state = 50, max_depth=3)
classifier1.fit(X_train, y_train)

classifier2= DecisionTreeClassifier(criterion = "entropy", random_state = 50, max_depth=3)
classifier2.fit(X_train, y_train)

y_train_pred1 = classifier1.predict(X_train)
print ("Accuracy of train data is ", accuracy_score(y_train,y_train_pred1)*100)

y_train_pred2 = classifier2.predict(X_train)
print ("Accuracy of train data is ", accuracy_score(y_train,y_train_pred2)*100)

y_pred1 = classifier1.predict(X_test)
y_pred1

y_pred2 = classifier2.predict(X_test)
y_pred2

print ("Accuracy for gini is ", accuracy_score(y_test,y_pred1)*100)
print ("Accuracy for entropy is ", accuracy_score(y_test,y_pred2)*100)

dot_data1 = tree.export_graphviz(classifier1, filled = True, rounded = True, out_file=None)
dot_data2 = tree.export_graphviz(classifier2, filled = True, rounded = True, out_file=None)

# Draw graph
# graph = pydotplus.graph_from_dot_data(dot_data)  
graph = graphviz.Source(dot_data1)
graph.render('dtree1')

graph = graphviz.Source(dot_data2)
graph.render('dtree2')

val_score1 = cross_val_score(classifier1, X_test, y_test, cv = 5)
accu1 = val_score1.mean()
print("cross validation for gini is : ", accu1)

val_score2 = cross_val_score(classifier2, X_test, y_test, cv = 5)
accu2 = val_score2.mean()
print("cross validation for entropy is : ", accu2)

# Show graph
# Image(graph.create_png())

