import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
#from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import accuracy_score
from sklearn import tree
from sklearn import preprocessing
#from sklearn import datasets
#import graphviz
from sklearn.ensemble import RandomForestClassifier
from sklearn import metrics
from sklearn.model_selection import cross_val_score
from sklearn.preprocessing import StandardScaler as sc

#reading file
data = pd.read_csv('/home/chitranshu/Documents/sonar.all-data', sep= ',', header= None)
#labeling the y values
label_encoder = preprocessing.LabelEncoder()
data.iloc[:,60]= label_encoder.fit_transform(data.iloc[:,60])
print(data.iloc[:,60].unique())
X = data.values[:, :60]
Y = data.values[:, 60]
#spliting the values in train and test data
X_train, X_test, y_train, y_test = train_test_split( X, Y, test_size = 0.20, random_state = 50, shuffle= True)

#regressor like a classifier
classifier = RandomForestClassifier(n_estimators=300, max_features=6, max_depth=8, min_samples_split=5, random_state=42) 
classifier.fit(X_train, y_train)  

#predict y value for test data
y_pred = classifier.predict(X_test)  
train_pred = classifier.predict(X_train)

print("Train Accuracy : ", metrics.accuracy_score(y_train,train_pred))
print("Accuracy:",metrics.accuracy_score(y_test, y_pred))
print("Precision:",metrics.precision_score(y_test, y_pred))
print("Recall:",metrics.recall_score(y_test, y_pred))
#error calcution and printing
print('Mean Absolute Error:', metrics.mean_absolute_error(y_test, y_pred))  
print('Mean Squared Error:', metrics.mean_squared_error(y_test, y_pred))  

val_score1 = cross_val_score(classifier, X_test, y_test, cv = 5)
accu1 = val_score1.mean()
print("cross validation for rf is : ", accu1)



